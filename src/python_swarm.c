#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "bmt.h"
#include "swarmfile.h"
#include "soc.h"


static void client_callback_do(const unsigned char *hash, const unsigned char *data, size_t data_length, void *callback_static) {
	PyObject *callback = (PyObject*)callback_static;

	PyObject_CallFunction(callback, "y#y#", hash, SWARM_WORD_SIZE, data, data_length);
}


static void soc_sign_callback(char *z, const unsigned char *address, const unsigned char *data, void *callback_static) {
	PyObject *callback = (PyObject*)callback_static;
	PyObject *r;
	unsigned char *signature_bytes;


	r = PyObject_CallFunction(callback, "y#y#", address, SWARM_ADDRESS_SIZE, data, SWARM_WORD_SIZE);
	signature_bytes =  PyBytes_AsString(r);

	memcpy(z, signature_bytes, SWARM_SIGNATURE_SIZE);
}


static bmt_spansize_t filehash_path(filehash_t *fctx, const unsigned char *filepath) {
	int fd;
	int r;
	int c;
	size_t l;
	struct stat st;
	unsigned char buf[SWARM_BLOCK_SIZE];

	fd = open(filepath, O_RDONLY);
	if (fd == -1) {
		return -1;
	}

	r = fstat(fd, &st);
	if (r == -1) {
		return -1;
	}
	
	// TODO: Return hardcoded zero-hash
	if (st.st_size == 0) {
		close(fd);
		return -1;
	}

	c = 0;
	while (1) {
		l = read(fd, buf, SWARM_BLOCK_SIZE);
		if (l == 0) {
			break;
		}
		c += l;
		filehash_write(fctx, buf, l);
	}

	close(fd);

	if (st.st_size != c) {
		return -1;
	}

	return filehash_sum(fctx);
}


static PyObject* method_bmt_hash(PyObject *self, PyObject *args) {
	bmt_t bctx;
	const unsigned char *input;
	Py_ssize_t input_length;
	bmt_spansize_t data_length;
	int r;

	r = PyArg_ParseTuple(args, "yIL", &input, &input_length, &data_length);
	if (r != 1) {
		PyErr_SetString(PyExc_ValueError, "could not build values");
		return NULL;
	}

	bmt_init(&bctx, (char*)input, input_length, data_length);
	r = bmt_sum(&bctx);
	if (r != 0) {
		PyErr_SetString(PyExc_RuntimeError, "bmt hashing failed");
		return NULL;
	}

	return Py_BuildValue("y#", &bctx.buf, SWARM_WORD_SIZE);
}


static PyObject* method_filehash_path(PyObject *self, PyObject *args) {
	filehash_t fctx;
	const unsigned char *inpath;
	PyObject *client_callback;

	int r;
	r = PyArg_ParseTuple(args, "s|O", &inpath, &client_callback);
	if (r != 1) {
		PyErr_SetString(PyExc_ValueError, "could not build values");
		return NULL;
	}

	if (client_callback == NULL) {
		filehash_init(&fctx);
	} else {
		filehash_init_callback(&fctx, client_callback_do, client_callback);
	}
	r = filehash_path(&fctx, inpath);
	if (r == -1) {
		PyErr_Format(PyExc_RuntimeError, "file hashing failed for path %s", inpath);
		return NULL;
	}

	return Py_BuildValue("y#", &fctx.buf, SWARM_WORD_SIZE);
}


static PyObject* method_soc_identifier(PyObject *self, PyObject *args) {
	int r;
	const PyBytesObject *topic;
	const PyBytesObject *index;
	const unsigned char *topic_bytes;
	const unsigned char *index_bytes;
	char out[128];

	r = PyArg_ParseTuple(args, "SS", &topic, &index);
	if (r != 1) {
		PyErr_SetString(PyExc_ValueError, "could not build values");
		return NULL;
	}

	topic_bytes = PyBytes_AsString((PyObject*)topic);
	index_bytes = PyBytes_AsString((PyObject*)index);
	r = soc_identifier(out, topic_bytes, index_bytes);
	if (r != 0) {
		PyErr_SetString(PyExc_RuntimeError, "could not build identifier");
		return NULL;
	}

	return Py_BuildValue("y#", &out, SWARM_WORD_SIZE);
}


static PyObject* method_soc_create(PyObject *self, PyObject *args) {
	int r;
	bmt_t bctx;
	const PyBytesObject *identifier;
	unsigned char *identifier_bytes;
	const PyBytesObject *input;
	const PyBytesObject *address;
	unsigned char *address_bytes;
	size_t input_length;
	unsigned char *p;
	unsigned char soc_hash[128];
	bmt_spansize_t data_length;
	unsigned char soc_serialized[SWARM_DATA_LENGTH_TYPESIZE + SWARM_BLOCK_SIZE];
	soc_chunk_t chunk;
	PyObject *client_callback;
	PyObject *keystore_callback;

	client_callback = NULL;

	r = PyArg_ParseTuple(args, "SSSILO|O", &identifier, &address, &input, &input_length, &data_length, &keystore_callback, &client_callback);
	if (r != 1) {
		PyErr_SetString(PyExc_ValueError, "could not build values");
		return NULL;
	}
	memcpy(&chunk.data.payload_sz, &input_length, sizeof(input_length));
	memcpy(chunk.data.span, &data_length, SWARM_DATA_LENGTH_TYPESIZE);
	if (data_length > SWARM_BLOCK_SIZE) {
		PyErr_Format(PyExc_ValueError, "data payload size %d exceeds maximum of %d bytes", chunk.data.payload_sz, SWARM_BLOCK_SIZE);
		return NULL;
	}
	chunk.data.payload = PyBytes_AsString((PyObject*)input);
	identifier_bytes = PyBytes_AsString((PyObject*)identifier);
	address_bytes = PyBytes_AsString((PyObject*)address);
	memcpy(chunk.identifier, identifier_bytes, SWARM_SOC_IDENTIFIER_SIZE);
	bmt_init(&bctx, (char*)chunk.data.payload, data_length, data_length);
	r = bmt_sum(&bctx);
	if (r != 0) {
		PyErr_SetString(PyExc_RuntimeError, "bmt hashing failed");
		return NULL;
	}
	memcpy(chunk.data.hash, bctx.buf, SWARM_WORD_SIZE);
	r = soc_digest(&chunk, soc_hash);
	if (r != 0) {
		PyErr_SetString(PyExc_RuntimeError, "soc digest failed");
		return NULL;
	}
	soc_sign_callback(&chunk.signature, address_bytes, soc_hash, keystore_callback);

	p = soc_serialize(&chunk, soc_serialized, &input_length);
	if (p == NULL) {
		PyErr_SetString(PyExc_RuntimeError, "soc serialize failed");
		return NULL;
	}

	if (client_callback != NULL) {
		client_callback_do(soc_hash, soc_serialized, input_length, client_callback);
	}

	return Py_BuildValue("y#y#y#", chunk.signature, SWARM_SIGNATURE_SIZE, soc_hash, SWARM_WORD_SIZE, soc_serialized, input_length);

}


static PyMethodDef SwarmMethods[] = {
	{"bmt_hash", method_bmt_hash, METH_VARARGS, "Calculate the BMT hash of the given data"},
	{"filehash_path", method_filehash_path, METH_VARARGS, "Calculate the Swarm file hash of the data from the given file path, with optional callback to receive chunks"},
	{"soc_identifier", method_soc_identifier, METH_VARARGS, "Build an SOC identifier from given topic and index"},
	{"soc_create", method_soc_create, METH_VARARGS, "Build an SOC chunk from given data and identifier"},
	{NULL, NULL, 0, NULL},
};


static struct PyModuleDef swarmmodule = {
	PyModuleDef_HEAD_INIT,
	"swarm",
	NULL,
	-1,
	SwarmMethods,
};


PyMODINIT_FUNC PyInit_swarm(void) {
	return PyModule_Create(&swarmmodule);
}
