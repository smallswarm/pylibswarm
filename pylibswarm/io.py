# standard imports
import os
import logging

logg = logging.getLogger(__name__)


class Outputter:

    def __init__(self, outdir, prepend_hash=False):
        self.outdir = outdir
        self.prepend_hash = prepend_hash
        os.makedirs(self.outdir, exist_ok=True)
        logg.info('outputter set to {}'.format(self.outdir))


    def dump(self, hsh, data):
        hsh_hex = hsh.hex()
        fp = os.path.join(self.outdir, hsh_hex)
        f = open(fp, 'wb')

        l = len(data)
        if self.prepend_hash:
            l += len(hsh) 
            f.write(hsh)
        f.write(data)
        f.close()

        logg.debug('wrote {} bytes for chunk file {}'.format(l, hsh_hex))
