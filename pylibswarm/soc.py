def from_str(s, target_length, label):
    if len(s) > target_length:
        raise ValueError('{} value {} exceeded max length {}'.format(label, s, target_length))
    b = s.encode('utf-8')
    fmt = '{' + ':<0{}s'.format(target_length * 2) + '}'
    return fmt.format(b.hex())
