# external imports
from chainlib.eth.cli import Wallet


class DefaultSigner(Wallet):

    def sign(self, address, message):
        address_hex = address.hex()
        signature = self.signer.sign_message(address_hex, message, dialect='eth')
        return signature
