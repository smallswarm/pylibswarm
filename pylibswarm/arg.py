# standard imports
import select
import sys


def stdin_arg():
    """Retreive input arguments from stdin if they exist.

    Method does not block, and expects arguments to be ready on stdin before being called.

    :rtype: str
    :returns: Input arguments string
    """
    h = select.select([sys.stdin.buffer], [], [])
    if len(h[0]) > 0:
        v = h[0][0].read()
        return v
    return None
