# standard imports
import argparse
import sys
import logging
import select

# local imports
from pylibswarm.arg import stdin_arg


logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

argparser = argparse.ArgumentParser()
argparser.add_argument('-n', action='store_true', help='skip newline at end of output')
argparser.add_argument('-b', action='store_true', help='output raw bytes')
argparser.add_argument('-l', dest='data_length', type=int, help='length of data represented by preimage')
argparser.add_argument('-v', action='store_true', help='verbose output')
argparser.add_argument('-u', action='store_true', help='treat input as utf-8 string')
argparser.add_argument('data', nargs='?', type=str, help='data input for BMT hasher')
largs = argparser.parse_args(sys.argv[1:])


if largs.v:
    logg.setLevel(logging.DEBUG)

data = largs.data
if data == None:
    data = stdin_arg()

input_data = data
if not largs.u:
    input_data.rstrip()
    if isinstance(input_data, str):
        input_data = data.encode('utf-8')

input_data_length = 0
input_data_length = len(input_data)
data_length = input_data_length
if largs.data_length != None:
    data_length = largs.data_length

logg.info('hashing {} bytes input with {} length prefix'.format(int(input_data_length), int(data_length)))


def main():
    # TODO: why does this segfault just one line before? - probably refcount related
    import swarm
    r = swarm.bmt_hash(input_data, input_data_length, data_length)

    if largs.b:
        sys.stdout.buffer.write(r[:32])
    else:
        s = '{}'.format(r[:32].hex())
        if not largs.n:
            s += '\n'
        sys.stdout.write(s)


if __name__ == '__main__':
    main()
