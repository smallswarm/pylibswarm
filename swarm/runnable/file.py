# standard imports
import os
import sys
import argparse
import logging

# local imports
from pylibswarm.io import Outputter


logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

argparser = argparse.ArgumentParser()
argparser.add_argument('-n', action='store_true', help='skip newline at end of output')
argparser.add_argument('-b', action='store_true', help='output raw bytes')
argparser.add_argument('--prepend-hash', dest='prepend_hash', action='store_true', help='prepend hash bytes to chunk output (no effect without -o)')
argparser.add_argument('-o', type=str, help='chunk output location')
argparser.add_argument('-v', action='store_true', help='verbose output')
argparser.add_argument('-vv', action='store_true', help='very verbose output')
argparser.add_argument('file', nargs='?', type=str, help='file to hash')
largs = argparser.parse_args(sys.argv[1:])


if largs.vv:
    logg.setLevel(logging.DEBUG)
elif largs.v:
    logg.setLevel(logging.INFO)

filepath = os.path.realpath(largs.file)

outputter = None
if largs.o:
    outputter = Outputter(largs.o, prepend_hash=largs.prepend_hash)

def main():
    import swarm

    if outputter:
        r = swarm.filehash_path(filepath, outputter.dump)
    else:
        r = swarm.filehash_path(filepath)

    if largs.b:
        sys.stdout.buffer.write(r[:32])
    else:
        s = '{}'.format(r[:32].hex())
        if not largs.n:
            s += '\n'
        sys.stdout.write(s)


if __name__ == '__main__':
    main()
