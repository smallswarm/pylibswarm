import os
from distutils.core import setup, Extension

#swarm_dir = "/home/lash/src/home/libswarm-ng"
root_dir = os.path.dirname(os.path.realpath(__file__))
default_swarm_dir = os.path.join(root_dir, 'aux', 'libswarm-ng')
swarm_dir = os.environ.get('LIBSWARM_DIR', default_swarm_dir)

requirements = []
f = open('requirements.txt', 'r')
while True:
    l = f.readline()
    if l == '':
        break
    requirements.append(l.rstrip())
f.close()


def main():
    setup(
        name="swarm",
        version="0.0.1a2",
        description="Swarm tooling",
        author="Louis Holbrook",
        author_email="dev@holbrook.no",
        install_requires=requirements,
        packages=[
            'pylibswarm',
            'pylibswarm.runnable',
            ],
        ext_modules=[
            Extension("swarm", [
                "src/python_swarm.c",
                os.path.join(swarm_dir, 'src/bmt.c'),
                os.path.join(swarm_dir, 'src/endian.c'),
                os.path.join(swarm_dir, 'src/swarmfile.c'),
                os.path.join(swarm_dir, 'src/chunk.c'),
                os.path.join(swarm_dir, 'src/keystore.c'),
                os.path.join(swarm_dir, 'src/soc.c'),
                os.path.join(swarm_dir, 'src/swarm.c'),
                os.path.join(swarm_dir, 'aux/keccak-tiny/keccak-tiny.c'),
                    ],
                include_dirs=[
                    os.path.join(swarm_dir, 'src'),
                    os.path.join(swarm_dir, 'aux/keccak-tiny'),
                    os.path.join(swarm_dir, 'aux/secp256k1/include'),
                    ],
                library_dirs=[
                    os.path.join(swarm_dir, 'aux/secp256k1/.libs'),
                ],
                runtime_library_dirs=[
                    os.path.join(swarm_dir, 'aux/secp256k1/.libs'),
                ],
                libraries=[
                    'secp256k1',
                    ],
                extra_compile_args=[
                    '-Dmemset_s(W,WL,V,OL)=memset(W,V,OL)',
                    ],
                ),
            ],
        entry_points = {
            'console_scripts': [
                'swarm-bmt=pylibswarm.runnable.bmt:main',
                'swarm-file=pylibswarm.runnable.file:main',
                'swarm-soc=pylibswarm.runnable.soc:main',
                ],
            },
        )

if __name__ == '__main__':
    main()
